using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class fakers : MonoBehaviour
{
    Transform[] t1;
    // Start is called before the first frame update
    void Start()
    {
        t1 = GetComponentsInChildren<Transform>();
        Debug.Log(t1.Length);
        for (int i = 5; i < t1.Length; i++)
        {
            t1[i].GetComponent<RawImage>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void chair()
    {
        for (int i = 1; i < t1.Length; i++)
        {
            if (i >= 1 && i < 5) { t1[i].GetComponent<RawImage>().enabled = true; }
            else
            {
                if (t1[i].GetComponent<RawImage>().enabled) { t1[i].GetComponent<RawImage>().enabled = false; }
            }

        }

    }
    public void sofa()
    {
        for (int i = 1; i < t1.Length; i++)
        {
            if (i >= 5 && i < 9) { t1[i].GetComponent<RawImage>().enabled = true; }
            else
            {
                if (t1[i].GetComponent<RawImage>().enabled) { t1[i].GetComponent<RawImage>().enabled = false; }
            }
        }

    }
    public void table()
    {
        for (int i = 1; i < t1.Length; i++)
        {
            if (i >= 9 && i < 13) { t1[i].GetComponent<RawImage>().enabled = true; }
            else
            {
                if (t1[i].GetComponent<RawImage>().enabled) { t1[i].GetComponent<RawImage>().enabled = false; }
            }

        }

    }
    public void misc()
    {
        for (int i = 1; i < t1.Length; i++)
        {
            if (i >= 13) { t1[i].GetComponent<RawImage>().enabled = true; }
            else
            {
                if (t1[i].GetComponent<RawImage>().enabled) { t1[i].GetComponent<RawImage>().enabled = false; }
            }

        }

    }

    public void productpage()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }




}
