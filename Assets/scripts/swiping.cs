using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class swiping : MonoBehaviour, IDragHandler, IEndDragHandler
{
    private Vector3 panelLocation;
    public float easing = 0.5f;
    Transform[] t;
    int previousbuttonvalue;




    // Start is called before the first frame update
    void Start()
    {
        previousbuttonvalue = 0;
        panelLocation = transform.position;
        t = GetComponentsInChildren<Transform>();
        Debug.Log(t.Length);
        //for(int i = 0; i<t.Length;i++)
        //{
        //    Debug.Log(t[i]);
        //}
        //Debug.Log(panelLocation);
        chair();



    }
    void Update()
    {

    }
    public void OnDrag(PointerEventData data)
    {
        float difference = data.pressPosition.x - data.position.x;
        // Debug.Log("press : " + data.pressPosition.x);
        //Debug.Log("pos: " + data.position.x);
        transform.position = panelLocation - new Vector3(difference, 0, 0);
        //Debug.Log("diff : " + difference);
    }
    public void OnEndDrag(PointerEventData data)
    {
        if (transform.position.x > 710)
        {
            Debug.Log("moved past 0");
            StartCoroutine(SmoothMove(transform.position, new Vector3(710, 1986, 0), easing));

        }
        else if (transform.position.x < 340)
        {
            Debug.Log("moved below 0");
            StartCoroutine(SmoothMove(transform.position, new Vector3(340, 1986, 0), easing));

        }
        else
        {
            panelLocation = transform.position;
        }

    }

    IEnumerator SmoothMove(Vector3 startpos, Vector3 endpos, float seconds)
    {
        float t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startpos, endpos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    public void chair()
    {
        click(1);


    }
    public void sofa()
    {
        click(3);


    }
    public void table()
    {
        click(5);


    }
    public void misc()
    {
        click(7);


    }

    void click(int x)
    {

        Color clickedColor, defaultcolor;
        ColorUtility.TryParseHtmlString("#FD6969", out clickedColor);
        ColorUtility.TryParseHtmlString("#FFFFFF", out defaultcolor);

        if (x != previousbuttonvalue)
        {
            Debug.Log("entered");
            ColorBlock c1 = t[x].GetComponent<Button>().colors;
            c1.normalColor = clickedColor;
            c1.highlightedColor = clickedColor;
            c1.pressedColor = clickedColor;
            c1.selectedColor = clickedColor;
            t[x].GetComponent<Button>().colors = c1;
            t[x + 1].GetComponent<TextMeshProUGUI>().color = Color.white;


            if (previousbuttonvalue != 0)
            {
                Debug.Log("exited");
                c1 = t[previousbuttonvalue].GetComponent<Button>().colors;
                c1.normalColor = defaultcolor;
                c1.highlightedColor = defaultcolor;
                c1.pressedColor = defaultcolor;
                c1.selectedColor = defaultcolor;
                t[previousbuttonvalue].GetComponent<Button>().colors = c1;
                t[previousbuttonvalue + 1].GetComponent<TextMeshProUGUI>().color = Color.black;

            }
        }

        previousbuttonvalue = x;
        //for (int i = 1; i < t.Length; i++)
        //{          
        //    if (i % 2 != 0)
        //    {
        //        ColorBlock c1 = t[i].GetComponent<Button>().colors;
        //        if (i == x)
        //        {                    

        //            c1.normalColor = clickedColor;
        //            c1.highlightedColor = clickedColor;
        //            c1.pressedColor = clickedColor;
        //            c1.selectedColor = clickedColor;
        //            t[i].GetComponent<Button>().colors = c1;

        //        }
        //        else
        //        {                    
        //            c1.normalColor = defaultcolor;
        //            c1.highlightedColor = defaultcolor;
        //            c1.pressedColor = defaultcolor;
        //            c1.selectedColor = defaultcolor;
        //            t[i].GetComponent<Button>().colors = c1;

        //        }
        //    }
        //    if (i % 2 == 0)
        //    {
        //        if (i == x+1)
        //        {
        //            t[i].GetComponent<TextMeshProUGUI>().color = Color.white;



        //        }
        //        else
        //        {
        //            t[i].GetComponent<TextMeshProUGUI>().color = Color.black;

        //        }


        //    }
        //}

        ////ColorBlock c1 = b1.colors;
        ////t1.color = Color.white;
        ////c1.normalColor = clickedColor;
        ////c1.highlightedColor = clickedColor;
        ////c1.pressedColor = clickedColor;
        ////c1.selectedColor = clickedColor;
        //b1.colors = c1;
    }
}