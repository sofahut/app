using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class scrolling_product : MonoBehaviour, IDragHandler, IEndDragHandler
{
    private Vector3 panelLocation;
    public float easing = 0.5f;



    // Start is called before the first frame update
    void Start()
    {
        panelLocation = transform.position;
        //Debug.Log(panelLocation);

    }
    void Update()
    {
        //Debug.Log(transform.position);
    }
    public void OnDrag(PointerEventData data)
    {

        float difference = data.pressPosition.y - data.position.y;

        // Debug.Log("press : " + data.pressPosition.x);
        //Debug.Log("pos: " + data.position.x);
        transform.position = panelLocation - new Vector3(0, difference, 0);
        //Debug.Log("diff : " + difference);




    }
    public void OnEndDrag(PointerEventData data)
    {




        if (transform.position.y < 684)
        {
            Debug.Log("moved past 0");
            StartCoroutine(SmoothMove(transform.position, new Vector3(540, 684, 0), easing));

        }
        else if (transform.position.y > 880)
        {
            Debug.Log("moved below 0");
            StartCoroutine(SmoothMove(transform.position, new Vector3(540, 880, 0), easing));

        }
        else
        {
            panelLocation = transform.position;
        }


    }

    IEnumerator SmoothMove(Vector3 startpos, Vector3 endpos, float seconds)
    {
        float t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startpos, endpos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

}