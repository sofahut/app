using UnityEngine;
using UnityEngine.SceneManagement;

public class product_faker : MonoBehaviour
{
    public GameObject gb;
    public Texture[] tx;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void change1()
    {
        gb.GetComponent<MeshRenderer>().material.mainTexture = tx[0];
    }
    public void change2()
    {
        gb.GetComponent<MeshRenderer>().material.mainTexture = tx[1];
    }
    public void change3()
    {
        gb.GetComponent<MeshRenderer>().material.mainTexture = tx[2];
    }
    public void change4()
    {
        gb.GetComponent<MeshRenderer>().material.mainTexture = tx[3];
    }

    public void nextscenes()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
