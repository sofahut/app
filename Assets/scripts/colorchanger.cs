using UnityEngine;
using UnityEngine.UI.ProceduralImage;

public class colorchanger : MonoBehaviour
{
    Renderer rend;
    //public Material m1;
    Transform[] gb;
    int previousbuttonvalue;
    // Start is called before the first frame update
    void Start()
    {
        //rend = GetComponent<Renderer>();
        //rend.enabled = true;
        //rend.sharedMaterial = m1;

        previousbuttonvalue = 0;
        gb = gameObject.GetComponentsInChildren<Transform>();
        gb[2].GetComponent<ProceduralImage>().enabled = false;
        gb[5].GetComponent<ProceduralImage>().enabled = false;
        gb[8].GetComponent<ProceduralImage>().enabled = false;
        gb[11].GetComponent<ProceduralImage>().enabled = false;
        color1();

    }

    // Update is called once per frame
    public void color1()
    {
        //m1.color = Color.red;
        higlighter(2);


    }
    public void color2()
    {
        //m1.color = Color.white;
        higlighter(5);

    }
    public void color3()
    {
        //m1.color = Color.blue;
        higlighter(8);

    }
    public void color4()
    {
        //m1.color = Color.green;
        higlighter(11);

    }

    void higlighter(int x)
    {
        if (x != previousbuttonvalue)
        {
            Debug.Log("entered");
            gb[x].GetComponent<ProceduralImage>().enabled = true;

            if (previousbuttonvalue != 0)
            {
                Debug.Log("exited");
                gb[previousbuttonvalue].GetComponent<ProceduralImage>().enabled = false;
            }
        }
        previousbuttonvalue = x;
    }
}
