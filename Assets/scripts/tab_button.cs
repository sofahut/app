using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class tab_button : MonoBehaviour
{
    Button b1;
    TextMeshProUGUI t1;
    // Start is called before the first frame update
    void Start()
    {
        b1 = GetComponent<Button>();
        t1 = b1.GetComponentInChildren<TextMeshProUGUI>();


    }

    public void click()
    {
        Color clickedColor;
        ColorUtility.TryParseHtmlString("#FD6969", out clickedColor);
        ColorBlock c1 = b1.colors;
        t1.color = Color.white;
        c1.normalColor = clickedColor;
        c1.highlightedColor = clickedColor;
        c1.pressedColor = clickedColor;
        c1.selectedColor = clickedColor;
        b1.colors = c1;


    }

}
